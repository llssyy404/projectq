﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Linq;
using System.Linq;

public class ResourceManager : MonoSingleton<ResourceManager>
{
    protected override void Init()
    {

    }

    public XDocument GetXml(XmlType xmlType)
    {
        TextAsset textAsset = Resources.Load<TextAsset>("Xml/" + xmlType.ToString().ToLower()) as TextAsset;
        
        if(textAsset == null)
        {
            // 에러처리
            return null;
        }

        return XDocument.Parse(textAsset.text);
    }

    // person Model Load는 소문자만 사용

    public GameObject LoadPerson(string personModelName)
    {
        var modelName = personModelName.Trim();
        var gameObject = Resources.Load<GameObject>("Prefabs/Person/" + modelName.ToLower()) as GameObject;

        if (gameObject == null)
        {
            Debug.LogError("LoadFail " + modelName);
        }
        
        return gameObject;
    }

    public GameObject LoadPerson(int classId)
    {
        string modelName = "";

        if(InfoManager.PersonScriptInfos.ContainsKey(classId))
        {
            modelName = InfoManager.PersonScriptInfos[classId].PrefPath;
        }
  
        var gameObject = Resources.Load<GameObject>("Prefabs/Person/" + modelName.ToLower()) as GameObject;

        if (gameObject == null)
        {
            Debug.LogError("LoadFail " + classId);
        }

        return gameObject;
    }

    public GameObject LoadPrefab(string prefabName)
    {
        var gameObject = Resources.Load<GameObject>("Prefabs/" + prefabName.ToString()) as GameObject;

        if (gameObject == null)
        {
            Debug.LogError("LoadFail " + prefabName);
        }

        return gameObject;
    }
}
