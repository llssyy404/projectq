﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
#endif


public enum SceneType
{
    Intro,
    Battle,
}


public class GameManager : MonoSingleton<GameManager>
{
    public SceneType currentScene { get; private set; }
    public static int ScreenBaseWidth = 1280;
    public static int ScreenBaseHeight = 720;

    protected override void Init()
    {
        InfoManager.Init();
        SoundManager.GetInstance().PlayLoopBgm("bgm");


        // 초기화된 씬 이름 저장
        var sceneName = SceneManager.GetActiveScene().name;
        currentScene = (SceneType)Enum.Parse(typeof(SceneType), sceneName);

        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;

        this.gameObject.AddComponent<FPSView>();
    }

    public void LoadScene(SceneType sceneType)
    {
        if (currentScene == sceneType) return;

        SceneManager.LoadScene(sceneType.ToString());
        currentScene = sceneType;
    }

    public void ExitApplication()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
