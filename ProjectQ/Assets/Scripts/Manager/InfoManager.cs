﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Linq;
using System.Linq;


public enum XmlType
{
    PersonScriptInfo,
    SkillScriptInfo,
}

public class InfoManager
{
    public static Dictionary<int, PersonScriptInfo> PersonScriptInfos { get; private set; }
    public static Dictionary<int, SkillScriptInfo> SkillScriptInfos { get; private set; }

    private static bool _isInitialized = false;

    public static void Init()
    {
        if (_isInitialized) return;

        var xPersonDoc = ResourceManager.GetInstance().GetXml(XmlType.PersonScriptInfo);
        PersonScriptInfos = xPersonDoc.Element("Category").Elements("PersonScriptInfo").Select(c => new PersonScriptInfo(c)).ToDictionary(c => c.ClassId);

        var xSkillDoc = ResourceManager.GetInstance().GetXml(XmlType.SkillScriptInfo);
        SkillScriptInfos = xSkillDoc.Element("Category").Elements("SkillScriptInfo").Select(c => new SkillScriptInfo(c)).ToDictionary(c => c.ClassId);

        _isInitialized = true;
    }

}
