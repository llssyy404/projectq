﻿using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    private static T _instance = null;
    public static T GetInstance()
    {
        if (_instance == null)
        {
            _instance = GameObject.FindObjectOfType(typeof(T)) as T;
            if (_instance == null)
            {
                _instance = new GameObject(typeof(T).ToString(), typeof(T)).GetComponent<T>();
            }
            if (!_isInitialized)
            {
                _isInitialized = true;
                _instance.Init();
            }
        }
        return _instance;
    }

    private static bool _isInitialized;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this as T;
        }
        else if (_instance != this)
        {
            DestroyImmediate(this);
            return;
        }
        if (!_isInitialized)
        {
            DontDestroyOnLoad(gameObject);
            _isInitialized = true;
            _instance.Init();
        }
    }

    protected abstract void Init();

    private void OnApplicationQuit()
    {
        _instance = null;
    }
}