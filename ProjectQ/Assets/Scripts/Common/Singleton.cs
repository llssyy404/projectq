﻿using System;

public abstract class Singleton<T> where T : class, new()
{
    private static volatile T instance;
    private static readonly object syncObject = new object();

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                lock (syncObject)
                {
                    if (instance == null)
                    {
                        instance = new T();
                    }
                }
            }
            return instance;
        }
    }
}
