﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    Based on the Public Domain MaxRectsBinPack.cpp source by Jukka Jylänki
     https://github.com/juj/RectangleBinPack/
*/
// MaxRectsBinPack 알고리즘 사용

public class PackingTexture
{


    // 일단 사용 가능한 방법만 오픈
    public enum FreeRectChoiceHeuristic
    {
        //RectBestShortSideFit,     ///< -BSSF: Positions the rectangle against the short side of a free rectangle into which it fits the best.
        //RectBestLongSideFit,      ///< -BLSF: Positions the rectangle against the long side of a free rectangle into which it fits the best.
        RectBestAreaFit,            //< -BAF: 들어갈 직사각형 중 남은 공간이 적은 곳을 찾아서 선택         
        //RectBottomLeftRule,       ///< -BL: Does the Tetris placement.
        //RectContactPointRule      ///< -CP: Choosest the placement where the rectangle touches other rects as much as possible.
    };


    // 사용하는 직사각형에 대한 정보 클래스
    public class RectInfo
    {
        public int StartX { get; private set; }
        public int StartY { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        public RectInfo(int startX, int startY, int width, int height)
        {
            StartX = startX;
            StartY = startY;
            Width = width;
            Height = height;
        }
    }


    private Dictionary<string, RectInfo> _useRects;
    private List<RectInfo> _freeRects;
    private int _texWidth;
    private int _texHeight;


    // !한번 텍스처 영역을 설정하면 사이즈를 변경 할 수 없다!
    public PackingTexture(int texWidth, int texHeight)
    {
        _useRects = new Dictionary<string, RectInfo>();
        _freeRects = new List<RectInfo>();
        _texWidth = texWidth;
        _texHeight = texHeight;

        _freeRects.Add(new RectInfo(0, 0, texWidth, texHeight));
    }

    public RectInfo Insert(string name, int width, int height, FreeRectChoiceHeuristic choiceBestRect = FreeRectChoiceHeuristic.RectBestAreaFit)
    {
        if (string.IsNullOrEmpty(name)) return null;
        if (width <= 0 || height <= 0) return null;
        if (width > _texWidth || height >_texHeight) return null;


        RectInfo newRectInfo = null;

        switch (choiceBestRect)
        {
            case FreeRectChoiceHeuristic.RectBestAreaFit:
                newRectInfo = GetRectInfoBestAreaFit(width, height);
                break;
            default:
                break;
        }

        if (newRectInfo == null)
        {
            // 더이상 들어갈 영역이 없을 경우            
            return null;
        }


        // 삽입 가능한 사각형과 freeRect영역과 겹쳐있는 곳을 찾아서 영역 분할
        int numRectanglesToProcess = _freeRects.Count;
        for (int i = 0; i < numRectanglesToProcess; ++i)
        {
            // 영역을 먼저 분할 한후 분할하기 전 영역을 제거한다.
            if (SplitFreeInfo(_freeRects[i], ref newRectInfo))
            {
                _freeRects.RemoveAt(i);
                --i;
                --numRectanglesToProcess;
            }
        }

        // 포함관계인 사각형을 찾아서 제거
        PruneFreeList();

        _useRects.Add(name, newRectInfo);

        return newRectInfo;
    }

    public RectInfo GetRectInfo(string name)
    {
        if(_useRects.ContainsKey(name))
        {
            return _useRects[name];
        }

        return null;
    }




    // 빈 사각형들 중에서 BAF 방식으로 삽입 가능한 사각형 정보를 반환
    private RectInfo GetRectInfoBestAreaFit(int width, int height)
    {
        if (_freeRects == null || _freeRects.Count <= 0) return null;


        int bestLeftArea = int.MaxValue;
        int bestStartX = 0;
        int bestStartY = 0;
        int bestWidth = 0;
        int bestHeight = 0;
        int bestShortSideFit = 0;


        foreach (var freeRect in _freeRects)
        {
            // 삽입 가능한 사각형이 빈 사각형보다 작은지
            if ((width <= freeRect.Width) && (height <= freeRect.Height))
            {
                int freeLeftArea = (freeRect.Width * freeRect.Height) - (width * height);
                int leftWidth = Mathf.Abs(freeRect.Width - width);
                int leftHeight = Mathf.Abs(freeRect.Height - height);
                int shortSideFit = Mathf.Min(leftWidth, leftHeight);

                // 조건 1 : 빈사각형에 삽입시 남은영역이 작은 것이 기준
                bool firstDiffer = bestLeftArea > freeLeftArea;
                // 조건 2 : 남은영역이 같을때 남은 변의 길이 중에 짧은 것이 기준
                bool secondDiffer = ((bestLeftArea == freeLeftArea) && (bestShortSideFit > shortSideFit));

                if (firstDiffer || secondDiffer)
                {
                    bestStartX = freeRect.StartX;
                    bestStartY = freeRect.StartY;
                    bestWidth = width;
                    bestHeight = height;
                    bestShortSideFit = shortSideFit;
                }
            }
        }

        return new RectInfo(bestStartX, bestStartY, bestWidth, bestHeight);
    }




    // 남은 사각형과 삽입되는 사각형의 겹치는 공간을 제외한 남는 공간을 분할
    private bool SplitFreeInfo(RectInfo freeNode, ref RectInfo usedNode)
    {

        // 두 사각형이 겹치지 않는 경우
        if (usedNode.StartX >= freeNode.StartX + freeNode.Width ||
             usedNode.StartX + usedNode.Width <= freeNode.StartX ||
             usedNode.StartY >= freeNode.StartY + freeNode.Height ||
             usedNode.StartY + usedNode.Height <= freeNode.StartY)
            return false;


        // x축으로 사각형이 접해있는지 확인한다.
        if (usedNode.StartX < freeNode.StartX + freeNode.Width && usedNode.StartX + usedNode.Width > freeNode.StartX)
        {
            // freeNode와 userNode가 겹치지 않는 freeNode 아래쪽 사각형 영역             
            if (usedNode.StartY > freeNode.StartY && usedNode.StartY < freeNode.StartY + freeNode.Height)
            {
                RectInfo newNode = new RectInfo(freeNode.StartX, freeNode.StartY, freeNode.Width, usedNode.StartY - freeNode.StartY);
                _freeRects.Add(newNode);
            }

            // freeNode와 userNode가 겹치지 않는 freeNode 위쪽 사각형 영역
            if (usedNode.StartY + usedNode.Height < freeNode.StartY + freeNode.Height)
            {
                RectInfo newNode = new RectInfo(freeNode.StartX, usedNode.StartY + usedNode.Height, freeNode.Width, freeNode.StartY + freeNode.Height - (usedNode.StartY + usedNode.Height));
                _freeRects.Add(newNode);
            }
        }

        // y축으로 사각형이 접해있는지 확인한다.
        if (usedNode.StartY < freeNode.StartY + freeNode.Height && usedNode.StartY + usedNode.Height > freeNode.StartY)
        {
            // freeNode와 userNode가 겹치지 않는 freeNode 왼쪽 사각형 영역
            if (usedNode.StartX > freeNode.StartX && usedNode.StartX < freeNode.StartX + freeNode.Width)
            {
                RectInfo newNode = new RectInfo(freeNode.StartX, freeNode.StartY, usedNode.StartX - freeNode.StartX, freeNode.Height);
                _freeRects.Add(newNode);
            }

            // freeNode와 userNode가 겹치지 않는 freeNode 오른쪽 사각형 영역
            if (usedNode.StartX + usedNode.Width < freeNode.StartX + freeNode.Width)
            {
                RectInfo newNode = new RectInfo(usedNode.StartX + usedNode.Width, freeNode.StartY, freeNode.StartX + freeNode.Width - (usedNode.StartX + usedNode.Width), freeNode.Height);
                _freeRects.Add(newNode);
            }
        }

        return true;
    }

    // freeNode 중에 포함 관계인 사각형을 찾아서 제거 
    private void PruneFreeList()
    {
        for (int i = 0; i < _freeRects.Count; ++i)
            for (int j = i + 1; j < _freeRects.Count; ++j)
            {
                if (IsContainedIn(_freeRects[i], _freeRects[j]))
                {
                    _freeRects.RemoveAt(i);
                    --i;
                    break;
                }
                if (IsContainedIn(_freeRects[j], _freeRects[i]))
                {
                    _freeRects.RemoveAt(j);
                    --j;
                }
            }
    }


    // 두 사각형이 포함 관계인지 확인
    private bool IsContainedIn(RectInfo r1, RectInfo r2)
    {
        return  r1.StartX               >= r2.StartX               &&
                r1.StartY               >= r2.StartY               &&
                r1.StartX + r1.Width    <= r2.StartX + r2.Width    &&
                r1.StartY + r1.Height   <= r2.StartY + r2.Height;
    }
}
