﻿using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshCombinerMono : MonoBehaviour
{ 


    public class MeshInfo
    {
        public MeshFilter meshFilter { get; set; }
        public MeshRenderer meshrenderer { get; set; }
    }


    void Start()
    {
        CombineMesh();
    }


    private void CombineMesh()
    {
        var targetMapObject = this.gameObject;

        var newGameObject = new GameObject();
        newGameObject.name = "MeshCombiner";
        MeshFilter[] meshFilters = targetMapObject.transform.GetComponentsInChildren<MeshFilter>();
        if (meshFilters == null || meshFilters.Length == 0) return;
        MeshRenderer[] meshRenderers = targetMapObject.transform.GetComponentsInChildren<MeshRenderer>();


        var meshfilter = newGameObject.AddComponent<MeshFilter>();
        var meshRenderer = newGameObject.AddComponent<MeshRenderer>();








        // texture combine
        var Textures = new Dictionary<string, Texture2D>();

        foreach (var meshs in meshRenderers)
        {
            if (meshs.material == null || meshs.material.mainTexture == null) continue;

            if (!Textures.ContainsKey(meshs.material.mainTexture.name))
            {
                Textures.Add(meshs.material.mainTexture.name, (Texture2D)meshs.material.mainTexture);
            }
        }



        // 아틀라스 화
        DynamicTextureAtlas atlas = new DynamicTextureAtlas(2048, 2048, TextureFormat.ARGB32);

        foreach (var texture in Textures)
        {
            atlas.AddImage(texture.Value, texture.Key);
        }
        atlas.ApplyTextureAtlas();




        // new Combine Material
        var combineMaterial = new Material(Shader.Find("Mobile/Diffuse"));
        combineMaterial.mainTexture = atlas.textureAtlas;





        // combine mesh
        var combineMesh = new Mesh();
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];
        int atlasWidth = atlas.Width;
        int atlasHeight = atlas.Height;

        for (int i = 0; i < meshFilters.Length; i++)
        {
            var mesh = meshFilters[i].mesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            var renderer = meshFilters[i].gameObject.GetComponent<MeshRenderer>();
            var rectInfo = atlas.GetImage(renderer.material.mainTexture.name);
            if (rectInfo == null) continue;



            // uv1 mapping
            var uv = new Vector2[meshFilters[i].mesh.uv.Length];

            for (int j = 0; j < uv.Length; ++j)
            {
                uv[j].x = Mathf.Lerp(rectInfo.StartX, rectInfo.StartX + rectInfo.Width, meshFilters[i].mesh.uv[j].x) / atlasWidth;
                uv[j].y = Mathf.Lerp(rectInfo.StartY, rectInfo.StartY + rectInfo.Height, meshFilters[i].mesh.uv[j].y) / atlasHeight;
            }
            mesh.uv = uv;



            //// uv2 mapping
            //var uv2 = new Vector2[meshFilters[i].mesh.uv2.Length];         
            //for (int j = 0; j < uv.Length; ++j)
            //{
            //    uv2[j].x = Mathf.Lerp(rectInfo.StartX, rectInfo.StartX + rectInfo.Width, meshFilters[i].mesh.uv2[j].x) / atlasWidth;
            //    uv2[j].y = Mathf.Lerp(rectInfo.StartY, rectInfo.StartY + rectInfo.Height, meshFilters[i].mesh.uv2[j].y) / atlasHeight;
            //}
            //mesh.uv2 = uv2;



            combine[i].mesh = mesh;
        }





        // complete
        combineMesh.CombineMeshes(combine);


        meshfilter.sharedMesh = combineMesh;
        meshRenderer.sharedMaterial = combineMaterial;


        // test code
        meshfilter.gameObject.layer = LayerMask.NameToLayer("Map");


        // 기존 메쉬렌더러 비활성화
        foreach (var meshs in meshRenderers)
        {
            meshs.enabled = false;
        }
    }

}