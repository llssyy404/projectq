﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// unity3d 5.6.3p1


public class DynamicTextureAtlas
{
    public Texture2D textureAtlas { get; private set; }
    public int Width { get; private set; }
    public int Height { get; private set; }

    private PackingTexture _packingTexture;


    public DynamicTextureAtlas(int width, int height, TextureFormat textureFormat = TextureFormat.ARGB32)
    {
        Width = width;
        Height = height;
        textureAtlas = new Texture2D(width, height, textureFormat, false);
        _packingTexture = new PackingTexture(width, height);
    }

    ~DynamicTextureAtlas()
    {

    }


    public void AddImage(Texture2D targetTexture, string textureName)
    {
        if (targetTexture == null) return;
        if (targetTexture.width == 0 || targetTexture.height == 0) return;
        if (string.IsNullOrEmpty(textureName)) return;
             
        var rectinfo = _packingTexture.Insert(textureName, targetTexture.width, targetTexture.height);
        if (rectinfo != null)
        {            
            textureAtlas.SetPixels32(rectinfo.StartX, rectinfo.StartY, rectinfo.Width, rectinfo.Height, targetTexture.GetPixels32());
        }        
    }

    public void ApplyTextureAtlas()
    {
        if (textureAtlas != null)
        {
            textureAtlas.Apply();
        }
    }

    public PackingTexture.RectInfo GetImage(string textureName)
    {
        return _packingTexture.GetRectInfo(textureName);
    }    
}
