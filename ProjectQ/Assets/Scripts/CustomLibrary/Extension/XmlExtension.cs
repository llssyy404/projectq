﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Linq;

public static class XmlExtension
{
    public static int GetInt(this XElement e, string attributeName)
    {
        int returnVal = 0;
        var attributeVal = e.Attribute(attributeName).Value;

        if (string.IsNullOrEmpty(attributeVal)) return returnVal;

        if (int.TryParse(attributeVal, out returnVal) == false)
        {
            // 에러일때 처리
        }

        return returnVal;
    }

    public static string GetString(this XElement e, string attributeName)
    {
        string returnVal = "";
        var attributeVal = e.Attribute(attributeName).Value;

        if (string.IsNullOrEmpty(attributeVal)) return returnVal;

        returnVal = attributeVal.Trim();
        return returnVal;
    }

    public static bool GetBool(this XElement e, string attributeName)
    {
        bool returnVal = false;
        var attributeVal = e.Attribute(attributeName).Value;

        if (string.IsNullOrEmpty(attributeVal)) return returnVal;

        if (bool.TryParse(attributeVal, out returnVal) == false)
        {
            // 에러일때 처리
        }

        return returnVal;
    }

    public static T GetEnum<T>(this XElement e, string attributeName)
    {
        T returnVal;
        var attributeVal = e.Attribute(attributeName).Value;

        if (string.IsNullOrEmpty(attributeVal)) return default(T);

        returnVal = (T)Enum.Parse(typeof(T), attributeVal);

        return returnVal;
    }
}
