﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum PersonType
{
    Player,
    Enemy,
    MaxPersonType
}

public class Person : MonoBehaviour
{
    public PersonInfo PersonInfo { get; protected set; }
    public ModelData ModelData { get; protected set; }
    public Person ChaserTarget { get; protected set; }
    public Vector3 DestMovePos { get; protected set; }
    public AudioSource _audioSource { get; protected set; }
    public PersonType PersonType { get; protected set; }

    protected Animator _anim;
    protected float _speed = 5.0f;
    protected AnimatorStateInfo _currentBaseState;
    protected float _animElapsedTime = 0.0f;
    protected NavMeshAgent _nav = null;
    protected Collider _weaponCollider = null;

    private BattlePersonUI _battlePersonUI;

    // trail
    protected WeaponTrail weaponTrail;
    private float t = 0.033f;
    private float m = 0;
    private Vector3 lastEulerAngles = Vector3.zero;
    private Vector3 lastPosition = Vector3.zero;
    private Vector3 eulerAngles = Vector3.zero;
    private Vector3 position = Vector3.zero;
    private float tempT = 0;
    private bool gatherDeltaTimeAutomatically = true; // ** You may want to set the deltaTime yourself for personal slow motion effects
    private float animationIncrement = 0.003f; // ** This sets the number of time the controller samples the animation for the weapon trails

    // 스킬
    public SkillHandler SkillHandler { get; protected set; }

    // unity Function : 상속받지 않고 단독 실행할때만 실행됨

    void Start()
    {
        //Init();
    }

    void Update()
    {
        UpdateFSM();
    }

    protected virtual void LateUpdate()
    {
        if (weaponTrail == null)
            return;

        if (gatherDeltaTimeAutomatically)
        {
            t = Mathf.Clamp(Time.deltaTime, 0, 0.066f);
        }

        RunTrail();
    }

    public virtual void Init(int classId = 0)
    {
        SetPersonInfo(classId);
        InitInner();
    }

    public virtual void Init(PersonInfo personInfo)
    {
        SetPersonInfo(personInfo);
        InitInner();
    }

    private void InitInner()
    {
        _anim = GetComponent<Animator>();
        _nav = GetComponent<NavMeshAgent>();
        ModelData = GetComponent<ModelData>();
        InitFSMState();
        SkillHandler = gameObject.AddComponent<SkillHandler>();
        SkillHandler.Init(this);
    }

    public void SetPersonInfo(PersonInfo personInfo)
    {
        if (personInfo == null)
        {
            Debug.Log("Person Info is None");
            return;
        }
        PersonInfo = personInfo;
    }

    public void SetPersonInfo(int classId = 0)
    {
        PersonInfo = new PersonInfo();
        // 인덱스로 스크립트 정보 얻어와서 정보세팅
        if (classId == 0)
        {
            Debug.Log("Person ClassId == 0");
            return;
        }

        PersonScriptInfo personScriptInfo = InfoManager.PersonScriptInfos[classId];
        PersonInfo.Set(personScriptInfo);
    }

    public void InitBattlePersonUI(BattlePersonUI battlePersonUI, Vector3 offset = new Vector3())
    {
        _battlePersonUI = battlePersonUI;
        _battlePersonUI.Init(this.transform, offset);
    }

    public void Init3DMark()
    {
        GameObject uiPref = ResourceManager.GetInstance().LoadPrefab("3DMarkUI");
        var uiObj = Instantiate(uiPref, transform.position, Quaternion.identity);
        uiObj.transform.SetParent(transform);
        uiObj.transform.localScale = transform.localScale * 0.3f;
    }

    public virtual bool Move(Vector3 dir)
    {
        if (IsDead())
            return false;

        if (GetCurrentState() == FSM_State_Type.Attack)
            return false;


        if (dir.x == 0.0f && dir.y == 0.0f)
        {
            ChangeFSMState(FSM_State_Type.Idle);
            return false;
        }

        ChangeFSMState(FSM_State_Type.Move);

        transform.eulerAngles = new Vector3(transform.eulerAngles.x,
                Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg, transform.eulerAngles.z);
        transform.Translate(Vector3.SqrMagnitude(dir) * Vector3.forward * Time.deltaTime * _speed);


        _anim.SetFloat("Speed", Vector3.SqrMagnitude(dir));
        _anim.SetFloat("Direction", Mathf.Atan2(dir.x, dir.y) * 180.0f / 3.14159f);

        _nav.isStopped = true;

        return true;
    }

    public bool CheckMove()
    {
        if (IsDead())
            return false;

        if (GetCurrentState() == FSM_State_Type.Attack)
            return false;

        if (ChaserTarget == null)
            return false;

        return true;
    }

    public virtual bool Move()
    {
        if (DestMovePos == null)
            return false;

        _anim.SetFloat("Speed", 1.0f);
        _nav.isStopped = false;
        _nav.SetDestination(DestMovePos);
        return true;
    }

    public virtual bool Idle()
    {
        _anim.SetFloat("Speed", 0.0f);
        _nav.isStopped = true;

        return true;
    }

    public virtual bool Chaser()
    {
        if (ChaserTarget == null)
            return false;

        _anim.SetFloat("Speed", 1.0f);
        _nav.isStopped = false;
        _nav.SetDestination(ChaserTarget.transform.position);

        return true;
    }

    public bool CheckAttack()
    {
        if (IsDead())
            return false;

        return true;
    }

    public virtual bool Attack()
    {
        if (IsDead())
            return false;

        _anim.SetFloat("Speed", 0.0f);

        if (ChaserTarget) transform.LookAt(ChaserTarget.transform);

        _nav.isStopped = true;

        return true;
    }

    public virtual bool Attacking()
    {
        return true;
    }

    public virtual bool AttackEnd()
    {
        return true;
    }

    public virtual bool Damaged()
    {
        return true;
    }

    public virtual bool Death()
    {
        _anim.Play("Die");
        _weaponCollider.enabled = false;
        weaponTrail.ClearTrail();
        weaponTrail.SetTime(0.0f, 0, 1);

        return true;
    }

    public virtual bool IsDead()
    {
        if (PersonInfo.hp <= 0)
            return true;

        return false;
    }

    public void ApplyDamaged(int damage)
    {
        if (IsDead())
            return;

        PersonInfo.hp -= damage;
        Manager.SetUINumber(damage, transform.position);

        if (_battlePersonUI != null)
        {
            _battlePersonUI.SetHpBar(PersonInfo.hp / PersonInfo.maxHp);
        }

        if (PersonInfo.hp <= 0)
            ChangeFSMState(FSM_State_Type.Death);
        else
            Damaged();
    }

    public void RunTrail()
    {
        if (t <= 0.0f)
            return;

        eulerAngles = weaponTrail.gameObject.transform.localEulerAngles;
        position = weaponTrail.gameObject.transform.localPosition;

        while (tempT < t)
        {
            tempT += animationIncrement;
            m = tempT / t;
            weaponTrail.gameObject.transform.localEulerAngles = new Vector3(
                Mathf.LerpAngle(lastEulerAngles.x, eulerAngles.x, m), Mathf.LerpAngle(lastEulerAngles.y, eulerAngles.y, m), Mathf.LerpAngle(lastEulerAngles.z, eulerAngles.z, m));
            weaponTrail.gameObject.transform.localPosition = Vector3.Lerp(lastPosition, position, m);

            if (weaponTrail.time > 0)
            {
                weaponTrail.Itterate(Time.time - t + tempT);
            }
            else
            {
                weaponTrail.ClearTrail();
            }
        }

        tempT -= t;
        weaponTrail.gameObject.transform.localPosition = position;
        weaponTrail.gameObject.transform.localEulerAngles = eulerAngles;
        lastPosition = position;
        lastEulerAngles = eulerAngles;

        if (weaponTrail.time > 0)
        {
            weaponTrail.UpdateTrail(Time.time, t);
        }
    }

    // 이벤트함수
    void StartWeaponTrail()
    {
        weaponTrail.SetTime(1.5f, 0, 1);
        if (_weaponCollider) _weaponCollider.enabled = true;
    }

    void EndWeaponTrail()
    {
        weaponTrail.ClearTrail();
        weaponTrail.SetTime(0.0f, 0, 1);
        if (_weaponCollider) _weaponCollider.enabled = false;
    }


    // FSM State Machine
    public FSMStateMachine StateMachine { get; protected set; }

    public virtual void InitFSMState()
    {
        StateMachine = new FSMStateMachine();
        StateMachine.AddState(FSM_State_Type.Idle, new FSMStateIdle(), this);
        StateMachine.AddState(FSM_State_Type.Move, new FSMStateMove(), this, CheckMove);
        StateMachine.AddState(FSM_State_Type.Attack, new FSMStateAttack(), this, CheckAttack);
        StateMachine.AddState(FSM_State_Type.Death, new FSMStateDeath(), this);
        StateMachine.AddState(FSM_State_Type.Chaser, new FSMStateChaser(), this);
    }

    public void UpdateFSM()
    {
        if (StateMachine != null)
        {
            StateMachine.UpdateFSM();
        }
    }

    public FSM_State_Type GetCurrentState()
    {
        return StateMachine.GetFSMState();
    }

    public void ChangeFSMState(FSM_State_Type stateType)
    {
        StateMachine.ChangeState(stateType);
    }




    // battleManager 

    public BattleManager Manager { get; private set; }

    public void InitForBattleManager(BattleManager manager)
    {
        Manager = manager;
    }




    // sound

    public virtual IEnumerator SoundEffect()
    {
        yield break;
    }


}