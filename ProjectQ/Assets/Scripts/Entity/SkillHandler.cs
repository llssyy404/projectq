﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillHandler : MonoBehaviour
{
    public Person OwnPerson { get; private set; }
    public Dictionary<int, PersonSkill> PersonSkills { get; private set; }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Init(Person ownPerson)
    {
        PersonSkills = new Dictionary<int, PersonSkill>();

        OwnPerson = ownPerson;
    }

    public void AddSkill(int classId)
    {
        PersonSkill personSkill = gameObject.AddComponent<PersonSkill>();
        personSkill.Init(classId, OwnPerson);
        PersonSkills.Add(classId, personSkill);
    }

    public void SkillCoolTime()
    {
        foreach(KeyValuePair<int, PersonSkill> personSkill in PersonSkills)
        {
            float skillRemainTime = personSkill.Value.SkillRemainTime;
            if (skillRemainTime == 0.0f)
                continue;

            personSkill.Value.SetCoolTime(Mathf.Max(skillRemainTime - Time.deltaTime, 0.0f));
        }
    }

    bool CheckCoolTime(int classId)
    {
        if (PersonSkills[classId].SkillRemainTime != 0.0f)
        {
            Debug.Log("스킬" + classId + " 쿨타임: " + PersonSkills[classId].SkillRemainTime);
            return false;
        }

        return true;
    }

    public bool SetCoolTime(int classId)
    {
        if (!CheckCoolTime(classId))
            return false;

        PersonSkills[classId].SetMaxCoolTime();

        return true;
    }

    public float GetMaxCoolTime(int classId)
    {
        return InfoManager.SkillScriptInfos[classId].CoolTime;
    }

    public float GetRemainCoolTime(int classId)
    {
        return PersonSkills[classId].SkillRemainTime;
    }

    public void SkillStart(int classId, Vector3 position, Quaternion rotation)
    {
        GameObject skillObj = Instantiate(PersonSkills[classId].SkillPref, position, rotation);
        skillObj.SetActive(true);
        Skill skill = skillObj.GetComponent<Skill>();
        skill.Init(classId, PersonSkills[classId].Skill.OwnPerson);
        Destroy(skill.gameObject, skill.SkillInfo.playTime);
    }

    public AttackType GetUsableSkillType()
    {
        AttackType attackType = AttackType.BaseAttack;
        foreach (KeyValuePair<int, PersonSkill> personSkill in PersonSkills)
        {
            ++attackType;
            float skillRemainTime = personSkill.Value.SkillRemainTime;
            if (skillRemainTime != 0.0f)
                continue;

            return attackType;
        }

        return AttackType.BaseAttack;
    }
}
