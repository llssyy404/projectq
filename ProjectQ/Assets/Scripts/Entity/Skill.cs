﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ColliderType
{
    None,
    Capsule,
    Box,
    Sphere,
    MaxColliderType
}

public class Skill : MonoBehaviour
{
    public Person OwnPerson { get; set; }
    public SkillInfo SkillInfo { get; private set; }
    //private ColliderType _colliderType = ColliderType.Capsule;
    private SphereCollider _sphereCollider;
    private float speed = 5.0f;

	// Use this for initialization
	void Start () {
        //Init();
    }

    // Update is called once per frame
    void Update () {
        if (SkillInfo.isProjectile)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
        }
    }

    public void Init(int classId, Person ownPerson)
    {
        SkillInfo = new SkillInfo();
        if(classId == 0)
        {
            Debug.Log("Skill ClassId == 0");
            return;
        }

        SkillScriptInfo skillScriptInfo = InfoManager.SkillScriptInfos[classId];
        SkillInfo.Set(skillScriptInfo);
 
        _sphereCollider = GetComponent<SphereCollider>();
        _sphereCollider.radius = SkillInfo.colliderSize;

        OwnPerson = ownPerson;
    }
}
