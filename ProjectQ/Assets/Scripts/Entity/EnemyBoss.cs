﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBoss : Enemy
{
    private float _chaserDist;
    private float _findTaretDistance = 30.0f;
    private AttackType _attackType;


    public override void Init(int classId = 0)
    {
        base.Init(classId);
        InitInnerEnemyBoss();
    }

    public override void Init(PersonInfo personInfo)
    {
        base.Init(personInfo);
        InitInnerEnemyBoss();
    }

    private void InitInnerEnemyBoss()
    {
        SkillHandler.AddSkill(1);
        //SkillHandler.AddSkill(2);
        //SkillHandler.AddSkill(3);
    }

    // unity Function

    void Update()
    {
        UpdateEnemy();
        UpdateFSM();
        SkillHandler.SkillCoolTime();
    }

    public override bool Attack()
    {
        if (!base.Attack())
            return false;

        int skillId = GetCanSkillId(_chaserDist);

        if (skillId == 0)
        {
            if (_chaserDist <= PersonInfo.atkRange)
            {
                // 근접공격
                _attackType = AttackType.BaseAttack;
                _anim.SetBool("Attack1", true);
                _anim.SetBool("Skill1", false);
                weaponTrail.SetTime(_anim.GetCurrentAnimatorStateInfo(0).length, 0, 1);
            }
        }
        else
        {
            //스킬공격
            _attackType = AttackType.Skill1;
            _anim.SetBool("Attack1", false);
            _anim.SetBool("Skill1", true);
        }

        _animElapsedTime = 0.0f;
        return true;
    }

    public override bool Attacking()
    {
        _animElapsedTime += Time.deltaTime;
        if (_animElapsedTime >= _anim.GetCurrentAnimatorStateInfo(0).length)
        {
            StateMachine.ChangeState(FSM_State_Type.Idle);
        }
        return true;
    }

    public override bool AttackEnd()
    {
        _animElapsedTime = 0.0f;
        _anim.SetBool("Attack1", false);
        _anim.SetBool("Skill1", false);
        
        if (_weaponCollider && _attackType == AttackType.None) _weaponCollider.enabled = false;

        return true;
    }


    // 공격 가능한 거리 구하기
    private float GetCanAttackRange()
    {
        float attackRange = PersonInfo.atkRange;
        float remainSkillCoolTime = SkillHandler.GetRemainCoolTime(1);

        if (remainSkillCoolTime <= 0)
        {
            attackRange = InfoManager.SkillScriptInfos[1].Range;
        }

        return attackRange;
    }

    // 사용 가능한 스킬Id 얻기
    private int GetCanSkillId(float targetDist)
    {
        // 0 = skill 사용 불가
        int canSkillId = 0;

        foreach (var skill in SkillHandler.PersonSkills.Values)
        {
            // 스킬을 넣은 순서부터 먼저 사용가능함
            var skillInfo = skill.Skill.SkillInfo;
            bool BeInRangeTarget = targetDist <= skillInfo.range;
            if (skill.SkillRemainTime <= 0 && BeInRangeTarget)
            {
                canSkillId = skillInfo.classId;
                return canSkillId;
            }
        }

        return 0;
    }



    // 애니메이션 이벤트 함수
    public void Skill()
    {
        switch (_attackType)
        {
            case AttackType.Skill1:
                SkillHandler.SetCoolTime(1);
                SkillHandler.SkillStart(1, transform.position + transform.forward + new Vector3(0.0f, 1.0f), transform.rotation);
                break;
            default:
                break;
        }
    }



    // fsm

    public override void UpdateEnemy()
    {
        if (IsDead()) return;

        ChaserTarget = Manager.GetPlayer();
        _chaserDist = Vector3.Distance(transform.position, ChaserTarget.transform.position);
        float moveDist = Vector3.Distance(transform.position, DestMovePos);
        bool aliveTarget = !ChaserTarget.IsDead();


        bool canAttackProcess = _chaserDist <= GetCanAttackRange();
        bool canChaser = _chaserDist < _findTaretDistance;
        bool isListenTargetSound = _chaserDist <= _listenSoundDist;
        bool canDetect = IsVisibleTarget(ChaserTarget.transform.position) || _isHitByAttackDetect || isListenTargetSound;

        if (aliveTarget && canDetect && canAttackProcess)
        {
            if (IsTargetDiscoverFirst(StateMachine.GetFSMState(), FSM_State_Type.Attack))
            {
                StartNoticeEffect();
            }
            StateMachine.ChangeState(FSM_State_Type.Attack);
        }
        else if (aliveTarget && canDetect && canChaser)
        {
            if (IsTargetDiscoverFirst(StateMachine.GetFSMState(), FSM_State_Type.Chaser))
            {
                StartNoticeEffect();
            }
            StateMachine.ChangeState(FSM_State_Type.Chaser);
            _attackType = AttackType.None;
        }
        else
        {
            if (_canPatrol == false)
            {
                MoveProcess(moveDist);
                _attackType = AttackType.None;
            }
            else
            {
                PatrollProcess(moveDist);
                _attackType = AttackType.None;
            }
        }
    }


}
