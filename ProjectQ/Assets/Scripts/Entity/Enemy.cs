﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Person
{

    private MapEnemyData _mapEnemyData;
    private int _destPatrollIndex = 0;

    // 적마다 바뀔 수 있는 것들
    protected bool _canPatrol = false;
    protected bool _isHitByAttackDetect = false;    //공격을 받은 공격자를 인식하고 있는지

    // 게임 기본 설정
    protected readonly float _moveMinDist = 3.0f;     // 목표지점 오차 범위 거리
    protected readonly float _chaserMinDist = 10.0f;  // 추적하기 위한 최소 거리
    protected readonly float _restRate = 0.5f;        // 휴식하기 위한 확률 0 ~ 1
    protected readonly float _detectAngle = 75.0f;
    protected readonly float _hitByAttackDetectTime = 3.0f; // 공격받았을때 적을 강제적으로 인식하고 있는 시간
    protected readonly float _listenSoundDist = 5.0f;  // 소리를 듣는 거리
    private ParticleSystem _noticeEffect;

    // unity Function 

    void Start()
    {
        //Init();
    }

    void Update()
    {
        UpdateEnemy();
        UpdateFSM();
    }

    public override void Init(int classId = 0)
    {
        base.Init(classId);
        InitInnerEnemy();
    }

    public override void Init(PersonInfo personInfo)
    {
        base.Init(personInfo);
        InitInnerEnemy();
    }

    private void InitInnerEnemy(int classId = 0)
    {
        PersonType = PersonType.Enemy;

        _weaponCollider = ModelData.weaponCollider;
        if (_weaponCollider) _weaponCollider.enabled = false;

        weaponTrail = ModelData.weaponTrail;
        weaponTrail.SetTime(0.0f, 0, 1);

        DestMovePos = this.transform.position;

        if (_audioSource == null)
        {
            _audioSource = gameObject.AddComponent<AudioSource>();
            _audioSource.maxDistance = 10;
            _audioSource.spatialBlend = 1.0f;
            StartCoroutine(SoundEffect());
        }


        InitNoitceEffect();
    }

    public void InitMapEnemyData(MapEnemyData mapEnemyData)
    {
        _mapEnemyData = mapEnemyData;
        if (_mapEnemyData.patrollPoints != null && _mapEnemyData.patrollPoints.Length > 0)
        {
            _canPatrol = true;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "AttackCollider")
        {
            PlayerBaseAttack(other);
            PlayerSkill(other);
        }
    }

    public override bool Move()
    {
        if (!base.Move())
            return false;

        if (DestMovePos == null)
            return false;

        _anim.SetFloat("Speed", 1.0f);
        _nav.isStopped = false;
        _nav.SetDestination(DestMovePos);

        return true;
    }

    public override bool Attack()
    {
        if (!base.Attack())
            return false;

        _anim.SetBool("Attack1", true);
        _animElapsedTime = 0.0f;
        return true;
    }

    public override bool Attacking()
    {
        if (!base.Attacking())
            return false;

        _animElapsedTime += Time.deltaTime;
        if (_animElapsedTime >= _anim.GetCurrentAnimatorStateInfo(0).length)
        {
            StateMachine.ChangeState(FSM_State_Type.Idle);
        }
        return true;
    }

    public override bool AttackEnd()
    {
        if (!base.AttackEnd())
            return false;

        _animElapsedTime = 0.0f;
        _anim.SetBool("Attack1", false);
        if (_weaponCollider) _weaponCollider.enabled = false;

        return true;
    }

    private Vector3 GetNextPoint(int lastIndex)
    {
        if (_mapEnemyData == null) return DestMovePos;
        if (lastIndex < 0 || lastIndex >= _mapEnemyData.patrollPoints.Length) return _mapEnemyData.startingPoint.transform.position;

        var patrollPoints = _mapEnemyData.patrollPoints;
        int maxLength = patrollPoints.Length;
        var nextIndex = (lastIndex + 1) % maxLength;
        return _mapEnemyData.patrollPoints[nextIndex].transform.position;
    }

    // 충돌처리
    void PlayerBaseAttack(Collider other)
    {
        Player player = other.GetComponentInParent<Player>();
        if (player == null)
            return;

        ApplyDamaged(Random.Range(player.PersonInfo.atkMin, player.PersonInfo.atkMax));
        HitByAttack();
    }

    void PlayerSkill(Collider other)
    {
        Skill skill = other.GetComponent<Skill>();
        if (skill == null)
            return;

        if (skill.OwnPerson == null)
            return;

        if (skill.OwnPerson == this)
            return;

        if (skill.OwnPerson.PersonType != PersonType.Player)
            return;

        ApplyDamaged(Random.Range(skill.SkillInfo.atkMin, skill.SkillInfo.atkMax));
        HitByAttack();
    }

    // battleManager

    private Vector3 GetFindPlayerPosition()
    {
        return Manager.GetPlayerPosition();
    }



    // FSM State Machine

    public virtual void UpdateEnemy()
    {
        if (IsDead()) return;

        ChaserTarget = Manager.GetPlayer();
        float chaserDist = Vector3.Distance(transform.position, ChaserTarget.transform.position);
        float moveDist = Vector3.Distance(transform.position, DestMovePos);
        bool aliveTarget = !ChaserTarget.IsDead();
        bool canAttack = chaserDist >= 0 && chaserDist <= PersonInfo.atkRange;
        bool canChaser = chaserDist < _chaserMinDist;
        bool isListenTargetSound = chaserDist <= _listenSoundDist;
        bool canDetect = IsVisibleTarget(ChaserTarget.transform.position) || _isHitByAttackDetect || isListenTargetSound;

        if (aliveTarget && canDetect && canAttack)
        {
            if (IsTargetDiscoverFirst(StateMachine.GetFSMState(), FSM_State_Type.Attack))
            {
                StartNoticeEffect();
            }
            StateMachine.ChangeState(FSM_State_Type.Attack);
        }
        else if (aliveTarget && canDetect && canChaser)
        {
            if (IsTargetDiscoverFirst(StateMachine.GetFSMState(), FSM_State_Type.Chaser))
            {
                StartNoticeEffect();
            }
            StateMachine.ChangeState(FSM_State_Type.Chaser);
        }
        else
        {
            if (_canPatrol == false)
            {
                MoveProcess(moveDist);
            }
            else
            {
                PatrollProcess(moveDist);
            }
        }
    }


    protected void MoveProcess(float moveDist)
    {
        if (moveDist > _moveMinDist)
        {
            StateMachine.ChangeState(FSM_State_Type.Move);
        }
        else
        {
            StateMachine.ChangeState(FSM_State_Type.Idle);
        }
    }

    private float _patternTime = 0.0f;
    private float _patternEndTime = 5.0f;

    protected void PatrollProcess(float moveDist)
    {
        var currentState = StateMachine.GetFSMState();

        // 웨이포인트 변경
        if (moveDist <= _moveMinDist)
        {
            DestMovePos = GetNextPoint(_destPatrollIndex);
            _destPatrollIndex = (++_destPatrollIndex) % _mapEnemyData.patrollPoints.Length;

            StateMachine.ChangeState(FSM_State_Type.Idle);
            _patternTime = 0.0f;
            _patternEndTime = Random.Range(0.1f, 1.0f);
        }

        bool justChanged = currentState == FSM_State_Type.Attack || currentState == FSM_State_Type.Chaser;

        // 바로 상태가 변경되야 하는 경우
        if (justChanged)
        {
            StateMachine.ChangeState(FSM_State_Type.Move);
        }
        else
        {
            // 시간에 따라 행동결정 (확률계산)
            _patternTime += Time.deltaTime;
            if (_patternTime < _patternEndTime) return;
            _patternTime = 0.0f;

            float restRate = Random.value;
            if (restRate < _restRate)
            {
                StateMachine.ChangeState(FSM_State_Type.Idle);
                _patternEndTime = Random.Range(3.0f, 8.0f);
            }
            else
            {
                StateMachine.ChangeState(FSM_State_Type.Move);
                _patternEndTime = Random.Range(6.0f, 10.0f);
            }
        }
    }

    public bool IsVisibleTarget(Vector3 targetPos)
    {
        var forwardVec = transform.forward.normalized;
        forwardVec.y = 0;
        var targetVec = (targetPos - transform.position).normalized;
        targetVec.y = 0;

        // fowardVec 과  TagetVec의 내적으로 코사인 각도를 구한후 Deg로 변환
        float angle = Mathf.Acos(Vector3.Dot(forwardVec, targetVec)) * Mathf.Rad2Deg;
        bool detectTarget = angle < _detectAngle;

        return detectTarget;
    }


    private LTDescr _coHitTimer = null;

    public void HitByAttack()
    {
        if (_coHitTimer != null)
        {
            LeanTween.cancel(_coHitTimer.id);
            _coHitTimer = null;
        }

        _isHitByAttackDetect = true;
        _coHitTimer = LeanTween.value(0.0f, 1.0f, _hitByAttackDetectTime).setOnComplete(() =>
        {
            _coHitTimer = null;
            _isHitByAttackDetect = false;
        });
    }

    // 타겟을 처음 발견했는지 여부
    public bool IsTargetDiscoverFirst(FSM_State_Type currentState, FSM_State_Type nextState)
    {
        bool isTargetDiscoverFirst = false;

        switch (nextState)
        {
            case FSM_State_Type.Attack:
                isTargetDiscoverFirst = (currentState != FSM_State_Type.Attack);
                break;
            case FSM_State_Type.Chaser:
                isTargetDiscoverFirst = (currentState != FSM_State_Type.Chaser) && (currentState != FSM_State_Type.Attack);
                break;
            default:
                break;
        }

        return isTargetDiscoverFirst;
    }



    // sound 

    public override IEnumerator SoundEffect()
    {
        bool isDead = false;

        while (true)
        {
            if (GetCurrentState() == FSM_State_Type.Move || GetCurrentState() == FSM_State_Type.Chaser)
            {
                var walkSound = SoundManager.GetInstance().GetOneShotClip("Footsteps/Footstep_Gravel_3");
                _audioSource.PlayOneShot(walkSound);
                yield return new WaitForSeconds(0.5f);
            }
            else if (GetCurrentState() == FSM_State_Type.Death)
            {
                if (isDead == false)
                {
                    var deadSound = SoundManager.GetInstance().GetOneShotClip("Exertions/Mike_Death_15");
                    _audioSource.PlayOneShot(deadSound);
                    isDead = true;
                }
                yield return null;
            }
            else
            {
                yield return null;
            }

        }
    }



    // effect

    public void InitNoitceEffect()
    {
        var noticePrefabs = ResourceManager.GetInstance().LoadPrefab("ParticalDust_Blue");
        GameObject skillObj = Instantiate(noticePrefabs, transform.position + new Vector3(0, 2.5f, 0), Quaternion.identity);
        skillObj.transform.SetParent(transform);
        _noticeEffect = skillObj.GetComponent<ParticleSystem>();
        _noticeEffect.gameObject.SetActive(false);
    }

    public void StartNoticeEffect()
    {
        _noticeEffect.gameObject.SetActive(true);
        _noticeEffect.Play(true);
    }
}
