﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonSkill : MonoBehaviour
{
    public Skill Skill { get; private set; }
    public GameObject SkillPref { get; private set; }
    public float SkillRemainTime { get; private set; }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Init(int classId, Person ownPerson)
    {
        GameObject obj = ResourceManager.GetInstance().LoadPrefab(InfoManager.SkillScriptInfos[classId].PrefPath);
        SkillPref = Instantiate(obj);
        SkillPref.SetActive(false);
        Skill = SkillPref.GetComponent<Skill>();
        Skill.Init(classId, ownPerson);
        SkillRemainTime = 0.0f;
    }

    public void SetMaxCoolTime()
    {
        SkillRemainTime = InfoManager.SkillScriptInfos[Skill.SkillInfo.classId].CoolTime;
    }

    public void SetCoolTime(float coolTime)
    {
        SkillRemainTime = coolTime;
    }
}
