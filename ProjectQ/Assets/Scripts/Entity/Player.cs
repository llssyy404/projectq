﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackType
{
    None,
    BaseAttack,
    Skill1,
    Skill2,
    Skill3
}

public class Player : Person
{
    private AttackType _attackType;
    private const string _attackName = "Attack";
    public bool IsAuto { get; private set; }
    private bool _isCombo = false;
    
    // unity Function 

    void Start()
    {
        //Init();
    }

    void Update()
    {
        _currentBaseState = _anim.GetCurrentAnimatorStateInfo(0);   // set our currentState variable to the current state of the Base Layer (0) of animation

        if (CheckEndAttackTime())
        {
            EndAttackTime();
            return;
        }

        SkillHandler.SkillCoolTime();

        if (IsAuto)
        {
            UpdatePlayer();
        }

        UpdateFSM();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "AttackCollider")
        {
            EnemyBaseAttack(other);
            EnemySkill(other);
        }
    }
    //

    // Initialize
    public override void Init(int classId = 0)
    {
        base.Init(classId);
        InitInnerPlayer();
    }

    public override void Init(PersonInfo personInfo)
    {
        base.Init(personInfo);
        InitInnerPlayer();
    }

    private void InitInnerPlayer()
    {
        InitDrawBlade();
        PersonType = PersonType.Player;
        _attackType = AttackType.None;
        IsAuto = false;

        // 스킬 추가
        SkillHandler.AddSkill(1);
        SkillHandler.AddSkill(2);
        SkillHandler.AddSkill(3);

        _weaponCollider = ModelData.weaponCollider;
        if (_weaponCollider) _weaponCollider.enabled = false;

        weaponTrail = ModelData.weaponTrail;
        if (weaponTrail) weaponTrail.SetTime(0.0f, 0, 1);

        if (_audioSource == null)
        {
            _audioSource = gameObject.AddComponent<AudioSource>();
            _audioSource.maxDistance = 10;
            _audioSource.spatialBlend = 0.0f;
            StartCoroutine(SoundEffect());
        }
    }

    void InitDrawBlade()
    {
        ChangeFSMState(FSM_State_Type.Idle);
        _anim.SetFloat("Speed", 0.0f);
        _anim.SetBool("AttackStandy", true);
    }

    // behavior
    public bool CheckIdle()
    {
        if (IsDead())
            return false;

        if (GetCurrentState() == FSM_State_Type.Attack && _animElapsedTime != 0.0f)
            return false;

        return true;
    }

    public override bool Idle()
    {
        if (!base.Idle())
            return false;

        SetAttackType(AttackType.None);
        _anim.SetInteger("Attack", 0);
        _anim.SetBool("AttackStandy", true);
        return true;
    }

    public override bool Attack()
    {
        if (!base.Attack())
            return false;

        _anim.SetInteger(_attackName, (int)_attackType);
		PlayAttackOneShotSound();

        return true;
    }

    public override bool Damaged()
    {
        if (!base.Damaged())
            return false;

        Manager.ChangePlayerHp(PersonInfo.hp / PersonInfo.maxHp);
        Manager.battleCamera.SetShake(true,0.1f, 0.05f);
        Effect();
        return true;
    }

    public override bool Death()
    {
        if (!base.Death())
            return false;

        Manager.ChangePlayerHp(PersonInfo.hp / PersonInfo.maxHp);

        return true;
    }

    public override bool Move()
    {
        if (Manager.uiController.IsPushInput) return false;

        _anim.SetFloat("Speed", 1.0f);
        _nav.isStopped = false;
        _nav.SetDestination(DestMovePos);

        return true;
    }
    //

    //
    public void InputAttack()
    {
        if (GetCurrentState() == FSM_State_Type.Attack && _attackType != AttackType.BaseAttack)
            return;

        if (_animElapsedTime != 0 && _currentBaseState.length > _animElapsedTime)
        {
            _isCombo = true;
        }

        SetAttackType(AttackType.BaseAttack);
        ChangeFSMState(FSM_State_Type.Attack);
    }

    public void SetAttackType(AttackType attackType)
    {
        _attackType = attackType;
    }

    public void SetAuto(bool isAuto)
    {
        IsAuto = isAuto;
        if (!IsAuto)
        {
            if (GetCurrentState() == FSM_State_Type.Attack)
                return;

            ChangeFSMState(FSM_State_Type.Idle);
        }
    }

    bool CheckEndAttackTime()
    {
        if (GetCurrentState() != FSM_State_Type.Attack)
            return false;

        _animElapsedTime += Time.deltaTime;
        if (_currentBaseState.length > _animElapsedTime)
            return false;

        return true;
    }

    void EndAttackTime()
    {
        _animElapsedTime = 0.0f;
        if (CheckCombo())
        {
            int combo = _anim.GetInteger("Combo");
            _anim.SetInteger("Combo", ++combo);
            ChangeFSMState(FSM_State_Type.Attack);
			PlayAttackOneShotSound();
        }
        else
        {
            _anim.SetInteger("Combo", 0);
            if (_weaponCollider) _weaponCollider.enabled = false;

            ChangeFSMState(FSM_State_Type.Idle);
        }

        _isCombo = false;
    }

    bool CheckCombo()
    {
        if (_attackType != AttackType.BaseAttack)
            return false;

        if (!_isCombo)
            return false;

        int combo = _anim.GetInteger("Combo");
        if (combo >= 3)
            return false;

        return true;
    }

    private void UpdatePlayer()
    {
        if (IsDead())
            return;

        if (Manager.uiController.IsPushInput)
            return;

        if (GetCurrentState() == FSM_State_Type.Attack && _currentBaseState.length > _animElapsedTime)
            return;

        if (ChaserTarget == null || ChaserTarget.IsDead())
        {
            ChaserTarget = Manager.FindNearEnemy();
            if (ChaserTarget == null)
            {
                //ChangeFSMState(FSM_State_Type.Idle);
                return;
            }
        }

        float distance = Vector3.Distance(transform.position, ChaserTarget.transform.position);
        if (distance >= 0 && distance <= 2.0f)
        {
            AttackType usableAttackType = SkillHandler.GetUsableSkillType();
            UseSkill(usableAttackType);
        }
        else
        {
            ChangeFSMState(FSM_State_Type.Chaser);
        }
    }

    // 스킬 이벤트 함수
    void Skill()
    {
        switch (_attackType)
        {
            case AttackType.Skill1:
                SkillHandler.SkillStart(1, transform.position + transform.forward + new Vector3(0.0f, 1.0f), transform.rotation);
                break;
            case AttackType.Skill2:
                SkillHandler.SkillStart(2, transform.position + transform.forward + new Vector3(0.0f, 1.0f), transform.rotation);
                break;
            case AttackType.Skill3:
                SkillHandler.SkillStart(3, transform.position, transform.rotation);
                Manager.battleCamera.SetShake(true, 0.8f);
                break;
            default:
                break;
        }
    }

    public void UseSkill(AttackType attackType)
    {
        if (GetCurrentState() == FSM_State_Type.Attack)
            return;

        switch (attackType)
        {
            case AttackType.BaseAttack:
                break;
            case AttackType.Skill1:
                if(!SkillHandler.SetCoolTime(1)) return;
                break;
            case AttackType.Skill2:
                if(!SkillHandler.SetCoolTime(2)) return;
                break;
            case AttackType.Skill3:
                if(!SkillHandler.SetCoolTime(3)) return;
                break;
            default:
                return;
        }

        SetAttackType(attackType);
        ChangeFSMState(FSM_State_Type.Attack);
    }

    // 충돌처리
    void EnemyBaseAttack(Collider other)
    {
        Enemy enemy = other.GetComponentInParent<Enemy>();
        if (enemy == null)
            return;

        ApplyDamaged(Random.Range(enemy.PersonInfo.atkMin, enemy.PersonInfo.atkMax));
    }

    void EnemySkill(Collider other)
    {
        Skill skill = other.GetComponent<Skill>();
        if (skill == null)
            return;

        if (skill.OwnPerson == null)
            return;

        if (skill.OwnPerson == this)
            return;

        if (skill.OwnPerson.PersonType != PersonType.Enemy)
            return;

        ApplyDamaged(Random.Range(skill.SkillInfo.atkMin, skill.SkillInfo.atkMax));
    }

    public void Effect()
    {
        GameObject skillPref = ResourceManager.GetInstance().LoadPrefab("SkillAttack");
        GameObject skillObj = Instantiate(skillPref, transform.position + new Vector3(0, 1.0f, 0), Manager.battleCamera.transform.rotation);
        Destroy(skillObj, 0.5f);
    }

    // StateMachine
    public override void InitFSMState()
    {
        base.InitFSMState();
        StateMachine.AddState(FSM_State_Type.Idle, new FSMStateIdle(), this, CheckIdle);
        StateMachine.AddState(FSM_State_Type.Move, new FSMStateMove(), this, CheckMove);
        StateMachine.AddState(FSM_State_Type.Attack, new FSMStateAttack(), this, CheckAttack);
        StateMachine.AddState(FSM_State_Type.Death, new FSMStateDeath(), this);
    }



    // sound

    public override IEnumerator SoundEffect()
    {
        bool isDead = false;

        while (true)
        {
            if (GetCurrentState() == FSM_State_Type.Move || GetCurrentState() == FSM_State_Type.Chaser)
            {
                SoundManager.GetInstance().PlayOneShotClip("Footsteps/Footstep_Gravel_3");
                yield return new WaitForSeconds(0.5f);
            }
            else if (GetCurrentState() == FSM_State_Type.Death)
            {
                if (isDead == false)
                {
                    SoundManager.GetInstance().PlayOneShotClip("Exertions/Mike_Death_15");
                    isDead = true;
                }
                yield return null;
            }
            else
            {
                yield return null;
            }
        }
    }

	public void PlayAttackOneShotSound()
	{
		SoundManager.GetInstance().PlayOneShotClip("Exertions/Ed_Attack_1");
	}
}
