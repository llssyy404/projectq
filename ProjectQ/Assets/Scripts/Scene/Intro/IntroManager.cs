﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroManager : MonoBehaviour
{
    public void LoadBattleScene()
    {
        GameManager.GetInstance().LoadScene(SceneType.Battle);
    }
}
