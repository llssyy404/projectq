﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelData : MonoBehaviour
{
    public Collider weaponCollider;
    public WeaponTrail weaponTrail;
}
