﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberUI : MonoBehaviour
{
    public Text textComponent;

    private static float _endTime = 0.5f;
    private float _time = 0.0f;
    private static float _moveYSpeed = 100.0f;

    // Use this for initialization
    void Start()
    {
        _time = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        var textAnchoredPos = textComponent.rectTransform.anchoredPosition3D;
        textAnchoredPos += (new Vector3(0, _moveYSpeed, 0) * Time.deltaTime);
        textComponent.rectTransform.anchoredPosition3D = Vector3.Lerp(textComponent.rectTransform.anchoredPosition3D, textAnchoredPos, 0.8f);

        _time += Time.deltaTime;

        if (_time > _endTime)
        {
            Destroy(this.gameObject);
        }
    }
}
