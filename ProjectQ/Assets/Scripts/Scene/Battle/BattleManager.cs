﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    public enum State { Battle, Victory, Fail }
    public State BattleState { get; private set; }

    public MapRespawnPoint testRespawnPoint;
    public BattleCamera battleCamera;
    public BattleUIController uiController;


    private Player _player;
    private List<Enemy> _enemys;

    void Awake()
    {
        Init();
    }

    void Start()
    {
        StartCoroutine(UpdateBattleState());
    }

    void Update()
    {
        battleCamera.UpdateCamera();

        if (uiController != null)
        {
            uiController.UpdateUI();
        }
    }



    // Init

    private void Init()
    {
        battleCamera.Init();
        uiController.Init(this, MovePlayer, AttackPlayer, UseSkill0Player, UseSkill1Player, UseSkill2Player);
        LoadPersons();

        if (_player != null)
        {
            battleCamera.SetTarget(_player.transform);
        }
    }


    private void LoadPersons()
    {

        if (testRespawnPoint == null) return;

        // 플레이어 생성
        if (testRespawnPoint.playerPoint != null)
        {
            GameObject personPref = ResourceManager.GetInstance().LoadPerson(InfoManager.PersonScriptInfos[1].PrefPath);
            GameObject playerObj = Instantiate(personPref, testRespawnPoint.playerPoint.transform.position, Quaternion.identity);
            _player = playerObj.AddComponent<Player>();
            _player.Init(1);
            _player.InitBattlePersonUI(uiController.MakeBattlePersonUI(), new Vector3(0, 2.0f, 0));
            _player.InitForBattleManager(this);
            _player.Init3DMark();
        }


        // 적 생성
        _enemys = new List<Enemy>();

        if (testRespawnPoint.enemyDatas != null)
        {			
			for(int i = 0; i<testRespawnPoint.enemyDatas.Length; ++i)
            {
				if (i < testRespawnPoint.enemyDatas.Length - 1) {
                    int classId = 2;
                    var enemyTrans = testRespawnPoint.enemyDatas [i];
					GameObject personPref = ResourceManager.GetInstance ().LoadPerson (InfoManager.PersonScriptInfos [classId].PrefPath);
					GameObject enemyObj = Instantiate (personPref, enemyTrans.startingPoint.transform.position, Quaternion.identity);
					Enemy enemy = enemyObj.AddComponent<Enemy> ();
					enemy.Init (classId);
					enemy.InitForBattleManager (this);
					enemy.InitBattlePersonUI (uiController.MakeBattlePersonUI (), new Vector3 (0, 2.5f, 0));
					enemy.InitMapEnemyData (enemyTrans);
					_enemys.Add (enemy);
				} 
				else 
				{
                    int classId = 3;
					var enemyTrans = testRespawnPoint.enemyDatas [i];
					GameObject personPref = ResourceManager.GetInstance ().LoadPerson (InfoManager.PersonScriptInfos [classId].PrefPath);
					GameObject enemyObj = Instantiate (personPref, enemyTrans.startingPoint.transform.position, Quaternion.identity);
					enemyObj.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
					Enemy enemy = enemyObj.AddComponent<EnemyBoss> ();
					enemy.Init (classId);
					enemy.InitForBattleManager (this);
					enemy.InitBattlePersonUI (uiController.MakeBattlePersonUI (), new Vector3 (0, 2.5f, 0));
					enemy.InitMapEnemyData (enemyTrans);
					_enemys.Add (enemy);
				}
            }
        }

    }





    // playerController

    public void MovePlayer(Vector3 moveDir)
    {
        // 스틱을 떼거나 드래그할때 단위벡터로 호출됨
        // 업데이트 프레임 마다 전송
        //Debug.Log("MovePlayer" + moveDir);
        //_player.IsAuto = false;
        _player.Move(moveDir);
    }

    public void AttackPlayer()
    {
        _player.InputAttack();
    }

    public void UseSkill0Player()
    {
        _player.UseSkill(AttackType.Skill1);
    }
    public void UseSkill1Player()
    {
        _player.UseSkill(AttackType.Skill2);
    }
    public void UseSkill2Player()
    {
        _player.UseSkill(AttackType.Skill3);
    }

    public void SetAutoPlayer(bool isActive)
    {
        // 오토플레이어 설정
        Debug.Log("AutoPlay : " + isActive);
        _player.SetAuto(isActive);
        SetUIAutoPlayBtn(isActive);
    }







    // assist

    public Vector3 GetPlayerPosition()
    {
        if (_player == null) return Vector3.zero;

        return _player.transform.position;
    }

    public Player GetPlayer()
    {
        return _player;
    }



    // battleUIcontroller

    // hpValue : 0 ~ 1
    public void ChangePlayerHp(float hpValue)
    {
        uiController.ChangePlayerHp(hpValue);
    }

    public void SetUIAutoPlayBtn(bool isOn)
    {
        uiController.SetUIAutoPlayBtn(isOn);
    }

    public void SetUINumber(int number, Vector3 position)
    {
        uiController.SetUINumber(number, position, new Vector3(0, 1.5f, 0));
    }

    public bool IsPushEvent()
    {
        return uiController.IsPushInput;
    }

    public Enemy FindNearEnemy()
    {
        if (_enemys.Count == 0)
            return null;

        Enemy nearEnemy = null;
        float min = -1.0f;
        foreach (Enemy e in _enemys)
        {
            if (e.IsDead())
                continue;

            float dist = Vector3.Distance(_player.transform.position, e.transform.position);
            if (min == -1.0f || dist <= min)
            {
                nearEnemy = e;
                min = dist;
            }
        }

        return nearEnemy;
    }


    private IEnumerator UpdateBattleState()
    {
        // 스테이지 state 찾기
        while (true)
        {
            BattleState = GetBattleState();

            // state 처리
            switch (BattleState)
            {
                case State.Battle:
                    break;
                case State.Victory:
                    uiController.ShowResultBox(true);
                    yield break;
                case State.Fail:
                    uiController.ShowResultBox(false);
                    yield break;
                default:
                    break;
            }

            yield return new WaitForSeconds(1.0f);
        }// end while
    }

    private State GetBattleState()
    {
        if (_player != null && _player.PersonInfo.hp <= 0)
        {
            return State.Fail;
        }
        else
        {
            bool isExistEnemys = false;
            foreach (var enemy in _enemys)
            {
                if (enemy.PersonInfo.hp > 0)
                {
                    isExistEnemys = true;
                    break;
                }
            }

            if (isExistEnemys == true)
            {
                return State.Battle;
            }
            else
            {
                return State.Victory;
            }
        }

    }
}
