﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class UiVirtualPad : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    private enum PadType { Pad, Stick }

    public GameObject pad;
    public GameObject stick;

    private readonly float _screenWidth = Screen.width;
    private readonly float _screenHeight = Screen.height;
    private static float _scaleFactor = (float)Screen.height / GameManager.ScreenBaseHeight;
    private readonly float _offsetStickArea = 25.0f * _scaleFactor;
    private readonly float _offsetPadArea = 100.0f * _scaleFactor;

    private Vector3 _initPos = new Vector3(0, 0, 0);
    private float _limitMaxDistance = 75.0f * _scaleFactor;
    private float _limitMinDistance = 6.0f * _scaleFactor;
    private RectTransform _pad, _stick;
    private Action<Vector3, bool> _moveAction;
    private Vector3 _moveVector;
    private bool _isTouchPad = false;

    public void Init(Action<Vector3, bool> moveAction = null)
    {
        if (pad == null || stick == null)
        {
            Debug.LogWarning("VirtualPad Init fail");
            return;
        }

        _pad = pad.GetComponent<RectTransform>();
        _stick = stick.GetComponent<RectTransform>();

        if (moveAction != null)
        {
            _moveAction = moveAction;
        }
    }

    void Update()
    {
        if (_isTouchPad)
        {
            SendMoveAction(_moveVector, true);
        }
    }


    // event handler

    public void OnPointerDown(PointerEventData eventData)
    {
        _isTouchPad = true;
        StickDown(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _isTouchPad = false;
        StickUp();
        SendMoveAction(_moveVector, false);
    }

    public void OnDrag(PointerEventData eventData)
    {
        StickMove(eventData);
    }





    private void StickDown(PointerEventData eventData)
    {
        if (_pad == null || _stick == null) return;

        Vector3 mousePos = eventData.position;
        _stick.position = GetTouchPosClamp(PadType.Stick, mousePos);
        _pad.position = GetTouchPosClamp(PadType.Pad, mousePos);
    }

    private void StickUp()
    {
        if (_pad == null || _stick == null) return;

        // 앵커포지션을 기준으로 초기화
        _pad.anchoredPosition3D = _initPos;
        _stick.anchoredPosition3D = _initPos;
        _moveVector = Vector3.zero;
    }

    private void StickMove(PointerEventData eventData)
    {
        if (_pad == null || _stick == null) return;

        Vector3 dragPos = eventData.position;
        float stickDistance = Vector3.Distance(dragPos, _pad.position);

        // stick과 pad 거리가 Min보다 작으면 변화 없음
        if (stickDistance < _limitMinDistance) return;


        if (stickDistance >= _limitMaxDistance)
        {
            var dirNormalVec = (dragPos - _pad.position).normalized;
            _stick.position = _pad.position + (dirNormalVec * _limitMaxDistance);
        }
        else
        {
            _stick.position = dragPos;
        }

        // 이동벡터 계산
        Vector3 moveVector = (_stick.position - _pad.position) / _limitMaxDistance;
        _moveVector = moveVector;
    }

    private void SendMoveAction(Vector3 moveVector, bool isPushInput)
    {
        if (_moveAction != null)
        {
            _moveAction(moveVector, isPushInput);
        }
    }

    private Vector3 GetTouchPosClamp(PadType padType, Vector3 pos)
    {
        var offsetTouchArea = (padType == PadType.Pad) ? _offsetPadArea : _offsetStickArea;

        var xPos = Mathf.Clamp(pos.x, offsetTouchArea, _screenWidth / 2 - offsetTouchArea);
        var yPos = Mathf.Clamp(pos.y, offsetTouchArea, _screenHeight - offsetTouchArea);
        return new Vector3(xPos, yPos, 0);
    }
}