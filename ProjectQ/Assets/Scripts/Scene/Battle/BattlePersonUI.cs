﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattlePersonUI : MonoBehaviour
{

    public Image hpBar;

    private RectTransform _rectTrans;
    private Transform _targetTrans;
    private Vector3 _offset;

    public void Init(Transform targetTrans, Vector3 offset = new Vector3())
    {
        if (hpBar == null) return;
        hpBar.fillAmount = 1.0f;

        _rectTrans = GetComponent<RectTransform>();
        _targetTrans = targetTrans;
        _offset = offset;

        var viewPos = Camera.main.WorldToViewportPoint(_targetTrans.position + _offset);
        viewPos.x *= GameManager.ScreenBaseWidth;
        viewPos.y *= GameManager.ScreenBaseHeight;
        viewPos.z = 0;
        _rectTrans.anchoredPosition3D = viewPos;
    }

    public void SetHpBar(float hpPercent)
    {
        if (hpBar == null) return;

        float hpValue = Mathf.Clamp(hpPercent, 0.0f, 1.0f);
        hpBar.fillAmount = hpValue;
    }

    public void Update()
    {
        if (hpBar == null) return;
        if (_targetTrans == null) return;

        var viewPos = Camera.main.WorldToViewportPoint(_targetTrans.position + _offset);
        viewPos.x *= GameManager.ScreenBaseWidth;
        viewPos.y *= GameManager.ScreenBaseHeight;
        viewPos.z = 0;
        _rectTrans.anchoredPosition3D = Vector3.Lerp(_rectTrans.anchoredPosition3D, viewPos, 0.8f);
    }
}
