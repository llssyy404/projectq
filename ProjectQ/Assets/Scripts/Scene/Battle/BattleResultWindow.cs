﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleResultWindow : MonoBehaviour
{
    public Image backBg;
    public Text resultText;

    private Action _restartAction;

    public void Init(Action restartAction)
    {
        _restartAction = restartAction;
        // Init is Hide
        Hide();
    }

    public void Show(bool isVictory)
    {
        gameObject.SetActive(true);

        backBg.color = new Color(0, 0, 0, 0);

        LeanTween.value(backBg.gameObject, 0.0f, 0.8f, 3.0f)
         .setOnUpdate((float value) =>
         {
             backBg.color = new Color(0, 0, 0, value);
         })
         .setOnComplete(() =>
         {
             backBg.color = new Color(0, 0, 0, 0.8f);
         });


        if (isVictory)
        {
            resultText.text = "Victory";
        }
        else
        {
            resultText.text = "Fail";
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        resultText.text = "";
    }

    public void OnClickRestartBtn()
    {
        if(_restartAction != null)
        {
            _restartAction();
        }
    }
}
