﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleUIController : MonoBehaviour
{
    public bool IsPushInput { get; private set; } // 키 입력이 눌렸는지 확인하기 위한 Getter

    public GameObject backPanel;
    public Image hpGauge;
    public Toggle autoPlayBtn;
    public UiVirtualPad virualPad;
    public Image hitEffect;
    public Text skillCoolTime1;
    public Text skillCoolTime2;
    public Text skillCoolTime3;
    public Image skillCoolTimeBg1;
    public Image skillCoolTimeBg2;
    public Image skillCoolTimeBg3;


    public BattleResultWindow resultWindow;

    private BattleManager _manager;

    private Action<Vector3> _moveAction;
    private Action _attackAction;
    private Action _skillAction0;
    private Action _skillAction1;
    private Action _skillAction2;

    private Vector3 _moveKeyboardVector = new Vector3();
    private bool _isPushKeyboard = false;

    public void Init(BattleManager manager, Action<Vector3> moveAction, Action attackAction, Action skillAction0, Action skillAction1, Action skillAction2)
    {
        _manager = manager;

        virualPad.Init(MoveVector);

        if (moveAction != null)
        {
            _moveAction = moveAction;
        }

        if (attackAction != null)
        {
            _attackAction = attackAction;
        }

        if (skillAction0 != null)
        {
            _skillAction0 = skillAction0;
        }

        if (skillAction1 != null)
        {
            _skillAction1 = skillAction1;
        }

        if (skillAction2 != null)
        {
            _skillAction2 = skillAction2;
        }

        resultWindow.Init(ClickRestartBtn);

        SetHitEffect(false);
    }

    public void UpdateUI()
    {
        UpdateKeyboardInput();
        UpdateSkillCoolTime();
    }



    // interface

    public void MoveVector(Vector3 moveDirection, bool isPushInput)
    {
        if (_moveAction != null)
        {
            Vector3 resultVector = moveDirection;
            resultVector.x = Mathf.Clamp(resultVector.x, -1.0f, 1.0f);
            resultVector.y = Mathf.Clamp(resultVector.y, -1.0f, 1.0f);
            _moveAction(resultVector.normalized);

            IsPushInput = isPushInput;
        }
    }

    public BattlePersonUI MakeBattlePersonUI()
    {
        var prefab = ResourceManager.GetInstance().LoadPrefab("BattlePersonUI");
        var gameObject = Instantiate(prefab, backPanel.transform);
        return gameObject.GetComponent<BattlePersonUI>();
    }




    // onClick Event

    public void OnClickAttackBtn()
    {
        if (_attackAction != null)
        {
            IsPushInput = true;
            _attackAction();
            IsPushInput = false;
        }
    }

    public void OnClickSkillBtn_0()
    {
        if (_skillAction0 != null)
        {
            IsPushInput = true;
            _skillAction0();
            IsPushInput = false;
        }
    }

    public void OnClickSkillBtn_1()
    {
        if (_skillAction1 != null)
        {
            IsPushInput = true;
            _skillAction1();
            IsPushInput = false;
        }

    }

    public void OnClickSkillBtn_2()
    {
        if (_skillAction2 != null)
        {
            IsPushInput = true;
            _skillAction2();
            IsPushInput = false;
        }
    }

    public void OnClickAutoPlayBtn()
    {
        if (_manager == null) return;
        if (autoPlayBtn == null) return;

        _manager.SetAutoPlayer(autoPlayBtn.isOn);
    }

    public void ClickRestartBtn()
    {
        GameManager.GetInstance().LoadScene(SceneType.Intro);
    }

    public void ClickExitBtn()
    {
        GameManager.GetInstance().ExitApplication();
    }



    private void UpdateKeyboardInput()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");
        _moveKeyboardVector.x = horizontal;
        _moveKeyboardVector.y = vertical;
        _moveKeyboardVector.z = 0;
        _moveKeyboardVector = _moveKeyboardVector.normalized;

        if (vertical != 0.0f || horizontal != 0.0f)
        {
            MoveVector(_moveKeyboardVector, true);
            _isPushKeyboard = true;
        }

        if (_isPushKeyboard && _moveKeyboardVector == Vector3.zero)
        {
            MoveVector(_moveKeyboardVector, false);
            _isPushKeyboard = false;
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            OnClickAttackBtn();
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            OnClickSkillBtn_0();
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            OnClickSkillBtn_1();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            OnClickSkillBtn_2();
        }
    }

    private void UpdateSkillCoolTime()
    {
        if (_manager == null) return;
        var player = _manager.GetPlayer();
        if (player == null) return;

        var coolTime_1 = player.SkillHandler.GetRemainCoolTime(1);
        var coolTime_2 = player.SkillHandler.GetRemainCoolTime(2);
        var coolTime_3 = player.SkillHandler.GetRemainCoolTime(3);

        skillCoolTime1.text = coolTime_1> 0 ?  coolTime_1.ToString("0.#") : "";
        skillCoolTime2.text = coolTime_2 > 0 ? coolTime_2.ToString("0.#") : "";
        skillCoolTime3.text = coolTime_3 > 0 ? coolTime_3.ToString("0.#") : "";


        var coolTimeMax_1 = player.SkillHandler.GetMaxCoolTime(1);
        var coolTimeMax_2 = player.SkillHandler.GetMaxCoolTime(2);
        var coolTimeMax_3 = player.SkillHandler.GetMaxCoolTime(3);

        float skillPercent_1 = coolTime_1 / coolTimeMax_1;
        float skillPercent_2 = coolTime_2 / coolTimeMax_2;
        float skillPercent_3 = coolTime_3 / coolTimeMax_3;

        skillCoolTimeBg1.fillAmount = Mathf.Clamp(skillPercent_1, 0.0f, 1.0f);
        skillCoolTimeBg2.fillAmount = Mathf.Clamp(skillPercent_2, 0.0f, 1.0f);
        skillCoolTimeBg3.fillAmount = Mathf.Clamp(skillPercent_3, 0.0f, 1.0f);
    }



    public void ChangePlayerHp(float hpPercent)
    {
        SetUIHpBar(hpPercent);
        PlayHitEffect();
    }


    // ui

    private void SetUIHpBar(float hpPercent)
    {
        if (hpGauge == null) return;

        float hpValue = Mathf.Clamp(hpPercent, 0.0f, 1.0f);
        hpGauge.fillAmount = hpValue;
    }

    public void SetUIAutoPlayBtn(bool isOn)
    {
        if (autoPlayBtn == null) return;

        autoPlayBtn.isOn = isOn;
    }

    public void SetUINumber(int number, Vector3 position, Vector3 offset = new Vector3())
    {
        var numberPrefab = ResourceManager.GetInstance().LoadPrefab("NumberUI");
        var textObj = Instantiate(numberPrefab, backPanel.transform);

        var text = textObj.GetComponent<Text>();
        var rectTrans = text.rectTransform;
        var viewPos = Camera.main.WorldToViewportPoint(position + offset);
        viewPos.x *= GameManager.ScreenBaseWidth;
        viewPos.y *= GameManager.ScreenBaseHeight;
        viewPos.z = 0;
        rectTrans.anchoredPosition3D = viewPos;
        text.text = number.ToString();
    }

    public void PlayHitEffect()
    {
        if (hitEffect.gameObject.activeInHierarchy == true) return;

        SetHitEffect(true);
        hitEffect.color = new Color(1, 0, 0, 0);

        LeanTween.value(hitEffect.gameObject, 0.0f, 1.0f, 0.25f)
            .setOnUpdate((float value) => {
                hitEffect.color = new Color(1, 0, 0, value);
            })
            .setOnComplete(()=> {
                hitEffect.color = new Color(1, 1, 1, 0);
                SetHitEffect(false);
            }); 
    }

    private void SetHitEffect(bool isOn)
    {
        hitEffect.gameObject.SetActive(isOn);
    }




    // result Window

    public void ShowResultBox(bool isVictory)
    {
        resultWindow.Show(isVictory);
    }

}
