﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleCamera : MonoBehaviour
{
    private Transform _targetTrans;
    private Vector3 _cameraPos = new Vector3();
    private readonly Vector3 _offsetVector = new Vector3(0, 5, -5);
    private readonly float cameraSpeed = 10.0f;

    private bool _cameraShake = false;
    private float _cameraShakeTime = 1.0f;
    private float _shakeAmount = 0.1f;
    private float _shakeTime = 0.0f;

    public void Init()
    {

    }

    public void SetTarget(Transform targetTrans)
    {
        _targetTrans = targetTrans;
    }

    public void UpdateCamera()
    {
        if (_targetTrans == null) return;

        _cameraPos = _targetTrans.position + _offsetVector;
        this.transform.position = Vector3.Lerp(this.transform.position, _cameraPos, cameraSpeed * Time.deltaTime);

        if (_cameraShake == true)
        {
            this.transform.position = this.transform.position + Random.insideUnitSphere * _shakeAmount;

            _shakeTime += Time.deltaTime;

            if (_shakeTime > _cameraShakeTime)
            {
                _cameraShake = false;
                _shakeTime = 0.0f;
            }
        }

    }

    public void SetShake(bool isOn, float shakeTime = 1.0f, float shakeAmount = 0.1f)
    {
        _cameraShake = isOn;
        _cameraShakeTime = shakeTime;
        _shakeTime = 0.0f;
        _shakeAmount = shakeAmount;
    }
}
