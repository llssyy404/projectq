﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMStateMachine
{
    private Dictionary<FSM_State_Type , FSMState> _availableStates = new Dictionary<FSM_State_Type, FSMState>();

    private FSM_State_Type _currentStateType = FSM_State_Type.None;
    private FSMState _currentState = null;

    private FSM_State_Type _previousStateType = FSM_State_Type.None;

    //private bool isForceHolding = false;
    //private float _forceHoldingTime = 0.0f;
    //private float _forceTime = 0.0f;

    public void UpdateFSM()
    {
        if (_currentState != null)
        {
            _currentState.Stay();
        }
        
        //if(isForceHolding)
        //{
        //    _forceTime += Time.deltaTime;

        //    if (_forceTime > _forceHoldingTime)
        //    {
        //        isForceHolding = false;
        //        _forceTime = 0.0f;
        //        _forceHoldingTime = 0.0f;
        //    }
        //}

    }

    public void ChangeState(FSM_State_Type nextStateType/*, float forceHoldingTime=0.0f*/)
    {
        if (nextStateType == FSM_State_Type.None) return;
        if (_currentStateType == nextStateType) return;
        if(_availableStates.ContainsKey(nextStateType) == false)
        {
            Debug.LogError("No State :" + nextStateType);
            return;
        }


        // 포스타임 설정 되있을때 무시
        //if (isForceHolding)
        //{
        //    //Debug.Log("(ForceHolding)Ignore : " + nextStateType);
        //    return;
        //}



        // 다음 상태 조건 체크
        var nextState =_availableStates[nextStateType];
        if(nextState.IsInValidState != null && nextState.IsInValidState() == false)
        {
           // state 진입조건에 실패했으므로 상태변환에 실패
            return;
        }



        _previousStateType = _currentStateType;


        if(_currentState != null)
        {
            _currentState.Exit();
        }

        _currentStateType = nextStateType;
        _currentState = _availableStates[nextStateType];


        if (_currentState != null)
        {
            _currentState.Enter();
        }


        //// 강제 홀딩 타임 설정구간
        //if (forceHoldingTime > 0.0f)
        //{
        //    _forceHoldingTime = forceHoldingTime;
        //    _forceTime = 0.0f;
        //    isForceHolding = true;
        //}
    }


    public void AddState(FSM_State_Type fsmStateType , FSMState fsmState, Person person , Func<bool> isInvalidState = null)
    {
        if (fsmState == null) return;

        if (_availableStates.ContainsKey(fsmStateType))
        {
            _availableStates[fsmStateType] = fsmState;
            _availableStates[fsmStateType].Init(person);
        }
        else
        {
            _availableStates.Add(fsmStateType, fsmState);
            _availableStates[fsmStateType].Init(person);
        }
    }


    public FSM_State_Type GetFSMState()
    {
        return _currentStateType;
    }

    public FSM_State_Type GetPrevFSMState()
    {
        return _previousStateType;
    }

    public void ChangePrevFSMState()
    {
        if (_previousStateType != FSM_State_Type.None)
        {
            ChangeState(_previousStateType);
        }
    }

    public void SetInvaildState(FSM_State_Type type , Func<bool> isInvalid)
    {
        if (_availableStates.ContainsKey(type) == false)
        {
            Debug.Log("No State :" + type);
            return;
        }

      _availableStates[type].IsInValidState = isInvalid;
    }
}
