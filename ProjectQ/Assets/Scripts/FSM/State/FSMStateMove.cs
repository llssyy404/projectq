﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMStateMove : FSMState
{
    public override void Enter()
    {

    }

    public override void Stay()
    {
        Manager.Move();
    }

    public override void Exit()
    {

    }
}
