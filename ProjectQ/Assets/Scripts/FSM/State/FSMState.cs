﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum FSM_State_Type
{
    None,
    Idle,
    Move,
    Attack,
    Death,
    Chaser,
}

public class FSMState
{
    public Person Manager { private set; get; }
    
    public void Init(Person person)
    {
        Manager = person; 
    }

    public virtual void Enter() { }
    public virtual void Stay() { }
    public virtual void Exit() { }

    public Func<bool> IsInValidState;
}
