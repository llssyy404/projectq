﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMStateDeath : FSMState
{
    public override void Enter()
    {
        Manager.Death();
    }

    public override void Stay()
    {

    }

    public override void Exit()
    {

    }

}
