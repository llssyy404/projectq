﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMStateAttack : FSMState
{
    public override void Enter()
    {
        Manager.Attack();
    }

    public override void Stay()
    {
		Manager.Attacking();
    }

    public override void Exit()
    {
		Manager.AttackEnd();
    }
}
