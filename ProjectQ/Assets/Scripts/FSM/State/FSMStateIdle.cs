﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMStateIdle : FSMState
{

    public override void Enter()
    {
        Manager.Idle();
    }

    public override void Stay()
    {
        
    }

    public override void Exit()
    {

    }
}
