﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using UnityEngine;

public abstract class BaseInfo
{
    public int ClassId { get; private set; }

    protected BaseInfo(XElement e)
    {
        ClassId = e.GetInt("ClassId");
    }

}
