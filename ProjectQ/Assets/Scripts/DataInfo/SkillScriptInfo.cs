﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using UnityEngine;

public class SkillScriptInfo : BaseInfo
{
    public string Name { get; private set; }
    public string PrefPath { get; private set; }
    public int AtkMin { get; private set; }
    public int AtkMax { get; private set; }
    public int Range{ get; private set; }
    public int ColliderSize { get; private set; }
    public int PlayTime { get; private set; }
    public float CoolTime { get; private set; }
    public bool IsProjectile { get; private set; }

    public SkillScriptInfo(XElement e) : base(e)
    {
        Name = e.GetString("Name");
        PrefPath = e.GetString("PrefPath");
        AtkMin = e.GetInt("AtkMin");
        AtkMax = e.GetInt("AtkMax");
        Range = e.GetInt("Range");
        ColliderSize = e.GetInt("ColliderSize");
        PlayTime = e.GetInt("PlayTime");
        CoolTime = e.GetInt("CoolTime");
        IsProjectile = e.GetBool("IsProjectile");
    }
}
