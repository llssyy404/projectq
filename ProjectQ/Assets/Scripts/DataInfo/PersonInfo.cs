﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonInfo
{
    public int classId;
    public float hp;
    public float mp;
    public float maxHp;
    public float maxMp;
    public int level;
    public int atkMin;
    public int atkMax;
    public int atkRange;


    public void Set(PersonScriptInfo scriptInfo)
    {
        classId = scriptInfo.ClassId;
        hp = maxHp = scriptInfo.Hp;
        mp = maxMp = scriptInfo.Mp;
        level = scriptInfo.Level;
        atkMin = scriptInfo.AtkMin;
        atkMax = scriptInfo.AtkMax;
        atkRange = scriptInfo.AtkRange;
    }
}


