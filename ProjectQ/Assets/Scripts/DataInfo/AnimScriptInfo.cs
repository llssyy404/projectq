﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using UnityEngine;

public class AnimScriptInfo : BaseInfo
{
    public string Name { get; private set; }
    public string PersonType { get; private set; }
    public string StateType { get; private set; }
    public string Loop { get; private set; }
    public int PlayTime { get; private set; }

    public AnimScriptInfo(XElement e) : base(e)
    {
        Name = e.GetString("Name");
        PersonType = e.GetString("PersonType");
        StateType = e.GetString("StateType");
        Loop = e.GetString("Loop");
        PlayTime = e.GetInt("PlayTime");
    }
}