﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillInfo
{
    public int classId;
    public string name;
    public int atkMin;
    public int atkMax;
    public int range;
    public int colliderSize;
    public float playTime;
    public float coolTime;
    public bool isProjectile;

    public void Set(SkillScriptInfo scriptInfo)
    {
        classId = scriptInfo.ClassId;
        name = scriptInfo.Name;
        atkMin = scriptInfo.AtkMin;
        atkMax = scriptInfo.AtkMax;
        range = scriptInfo.Range;
        colliderSize = scriptInfo.ColliderSize;
        playTime = scriptInfo.PlayTime;
        coolTime = scriptInfo.CoolTime;
        isProjectile = scriptInfo.IsProjectile;
    }
}