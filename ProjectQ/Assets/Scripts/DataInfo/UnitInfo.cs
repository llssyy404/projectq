﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using UnityEngine;

public class UnitInfo : BaseInfo
{
    public string Name { get; private set; }

    public UnitInfo(XElement e) : base(e)
    {
        Name = e.GetString("Name");
    }
}
