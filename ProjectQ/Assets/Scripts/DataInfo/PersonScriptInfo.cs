﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using UnityEngine;

public class PersonScriptInfo : BaseInfo
{
    public string Name { get; private set; }
    public string PrefPath { get; private set; }
    public float Hp { get; private set; }
    public float Mp { get; private set; }
    public int Level { get; private set; }
    public int AtkMin { get; private set; }
    public int AtkMax { get; private set; }
    public int AtkRange { get; private set; }

    public PersonScriptInfo(XElement e) : base(e)
    {
        Name = e.GetString("Name");
        PrefPath = e.GetString("PrefPath");
        Hp = e.GetInt("Hp");
        Mp = e.GetInt("Mp");
        Level = e.GetInt("Level");
        AtkMin = e.GetInt("AtkMin");
        AtkMax = e.GetInt("AtkMax");
        AtkRange = e.GetInt("AtkRange");
    }
}