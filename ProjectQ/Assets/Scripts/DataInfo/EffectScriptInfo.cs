﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using UnityEngine;

public class EffectScriptInfo : BaseInfo
{
    public string Name { get; private set; }
    public string Loop { get; private set; }
    public int PlayTime { get; private set; }

    public EffectScriptInfo(XElement e) : base(e)
    {
        Name = e.GetString("Name");
        Loop = e.GetString("Loop");
        PlayTime = e.GetInt("PlayTime");
    }
}

